# Support

- [Slides](https://gitlab.com/azae/conferences/zemblanite/builds/artifacts/master/file/slides.pdf?job=compile_pdf)

# Lancer la compilation sous windows

1. Lancer docker

2. dans git bash :

    winpty  docker run -it --rm --entrypoint bash -v //d/azae:/data azae/latex

    cd /data/zemblanite

puis lancer la compilation avec :
    make pdf

3. dans le dossier "zemblanite"

  ouvrir le pdf avec chrome

4. dans bash :

q : quitter erreur
exit : sortir de docker / quitter bash

5. cloner un fichier de gitlab à mon atome :
Dans git bash
aller dans le dossier voulu (cd)
faire git clone (+url de git)
Dans atome
Add Project Folder

6. commandes git bash
cd aller vers
cd .. retourner au fichier parent
cd . rester là où on est
-

# Lancer la compilation sous linux

1. dans terminal:

    cd workspace/azae/conferences/zemblanite

    docker run --rm -it -v $(pwd):/data registry.gitlab.com/azae/outils/latex/cli make pdf


2. dans le dossier "zemblanite"

  ouvrir le pdf avec evince (dans mon dossier cloné)

3. dans termmial :

q : quitter erreur
exit : sortir de docker / quitter bash

5. cloner un fichier de gitlab dans mon workspace :
Dans terminal
Aller dans le dossier voulu (aller dans le dossier clique droit "ouvrir dans le terminal")
`git clone` (+url de git)
`git status` (pour savoir l'état des commit)
`git add .` `git add fichiersaajouter`
`git commit -m "nomducommentaire"`
`git push`
